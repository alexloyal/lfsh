<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'lfsh_dev');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Xr@Jj1*FxfJPbZaYfMJ+6U]rs/>q+*ToT1*x# g1NXD 9Sb6kh@ISc(r$FVTD&Fb');
define('SECURE_AUTH_KEY',  'r+w7s[~~12i&Jy-) mb}I*4D?T`U8$>,H o#|<VDwQ+?ItL_)yp5@,NHw!~|N1$g');
define('LOGGED_IN_KEY',    '9|56(lT=iC-ILe;h+g@/3m$Ux.arUM/C~jl+62tE[T$agSS~&C)[s.-Klnq4%bvh');
define('NONCE_KEY',        'Q7ae~?+JTSpP45V5:@7-z3WzV0qICsZlH:s~Vy!qDtfNLi,(P#8|!,m`R({<iqJ{');
define('AUTH_SALT',        'MX&hDOVF^m[{k1u]n>eGk8ZN|K{[GH--n&U>,N=+6|0 F{ /itFSe7rtZm_?i-8u');
define('SECURE_AUTH_SALT', '@$uR4ndQd*]HzZ00x-)5|`;!=#eywmf>T8LV.5rLI$6^5]4g7=:1n#1j:2W$G_U/');
define('LOGGED_IN_SALT',   '0dp`G;|O/N0Q~~!RQEQ=/jT7:/<^+ bb7@9+bEo=.WsT6*zX;.iU3x}Q3-XpNmAg');
define('NONCE_SALT',       '3L`R.&-!wXrOHhJ2l;NfL22ywbc**=5kC5yW?{,qzz3;#HACfWC4U>w<bgpSJJc&');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'lfsh_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
