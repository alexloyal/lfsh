<?php
/**
 * The Template for displaying all single posts.
 *
 * @package LFSH
 */

get_header(); ?>

	<div id="primary" class="content-area row">
	
		<footer id="footer" class="site-footer col_6" role="contentinfo">
				<div class="footer-menu col_4c">
					
					<div class="clf">
						<p>A project of the </p>
						<a href="http://www.clf.org" title="A project of the Conservation Law Foundation">
						<img src="<?php bloginfo('stylesheet_directory'); ?>/images/clf-logo-silo.png" alt="A project of the Conservation Law Foundation" />
					</a>
					</div>

					<?php wp_nav_menu( array( 'theme_location' => 'footer' ) ); ?>
				</div><!-- .site-info -->
			</footer>


		<main id="main" class="site-main col_6c" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'content', 'single' ); ?>

				 

			<?php endwhile; // end of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->

 
<?php get_footer(); ?>