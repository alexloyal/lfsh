<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package LFSH
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
	</header><!-- .entry-header -->
		

				<?php
					  if($post->post_parent)
					  $children = wp_list_pages("title_li=&child_of=".$post->post_parent."&echo=0");
					  else
					  $children = wp_list_pages("title_li=&child_of=".$post->ID."&echo=0");
					  if ($children) { ?>
					 <div class="children col_6">
					  <h3>Connect</h3>
					  <ul>
					  <?php echo $children; ?>
					  </ul>
					  </div>
					  <?php } ?>

			

	<div class="entry-content col_6c">
		<?php the_content(); ?>
		 
	</div><!-- .entry-content -->
	 
</article><!-- #post-## -->
