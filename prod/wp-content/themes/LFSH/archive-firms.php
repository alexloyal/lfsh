<?php
/**
 * The template for displaying Archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package LFSH
 */

get_header(); 


// $lfsh_firms_options = get_option( 'lfsh_firm_options' );


?>

	<section id="primary" class="content-area row skip-float">
		<footer id="footer" class="site-footer col_6" role="contentinfo">
				<div class="footer-menu col_4c">
					
					<div class="clf">
						<p>A project of the </p>
						<a href="http://www.clf.org" title="A project of the Conservation Law Foundation">
						<img src="<?php bloginfo('stylesheet_directory'); ?>/images/clf-logo-silo.png" alt="A project of the Conservation Law Foundation" />
					</a>
					</div>

					<?php wp_nav_menu( array( 
										'theme_location' => 'footer',
										'menu_id' => 'menu-footer-1'
											   ) ); ?>
				</div><!-- .site-info -->
			</footer>

		<main id="main" class="site-main col_6c" role="main">

		<?php if ( have_posts() ) : ?>

			<div class="page-header">
				<h1 class="page-title">
					Attorneys in our network
				</h1>


			</div><!-- .page-header -->

			<?php /* Start the Loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'content', 'firms-archive' ); ?>

				 

			<?php endwhile; // end of the loop. ?>

		 

		<?php else : ?>

			<?php get_template_part( 'content', 'none' ); ?>

		<?php endif; ?>

		</main><!-- #main -->
	</section><!-- #primary -->

 
<?php get_footer(); ?>
