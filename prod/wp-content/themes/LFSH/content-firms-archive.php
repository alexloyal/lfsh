<?php
/**
 * @package LFSH
 */
?>
<!-- archive -->
<article id="post-<?php the_ID(); ?>" <?php post_class('col_2'); ?>>
	<header class="entry-header">
		<?php // the_title( '<h2 class="entry-title">', '</h2>' ); ?>
	</header><!-- .entry-header -->
		 

	<div class="entry-content col_6c">
		<?php the_content(); ?>
		<div>

				<?php
					$firm_url= get_post_custom_values('firm_url'); 
					if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
					  ?><a href="<?php echo $firm_url[0]; ?>" target="_blank" title="<?php echo $title; ?>"><?php the_post_thumbnail(); ?></a><?php
					} 
					?>

		</div>	
		 
	</div><!-- .entry-content -->
	 
</article><!-- #post-## -->

