<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package LFSH
 */

get_header(); ?>

	<div id="primary" class="content-area row skip-float">
	
	<footer id="footer" class="site-footer col_6" role="contentinfo">
				<div class="footer-menu col_4c">
					
					<?php wp_nav_menu( array( 
										'theme_location' => 'footer',
										'menu_id' => 'menu-footer-1'
											   ) ); ?>

					<div class="clf">
						<p>A project of </p>
						<a href="http://www.clf.org" title="A project of the Conservation Law Foundation">
						<img src="<?php bloginfo('stylesheet_directory'); ?>/images/clf-logo-silo.png" alt="A project of the Conservation Law Foundation" />
					</a>
					</div>

					
				</div><!-- .site-info -->
			</footer>


		<main id="main" class="site-main col_6c" role="main">



			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'content', 'page' ); ?>

				 

			<?php endwhile; // end of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->

 
<?php get_footer(); ?>
